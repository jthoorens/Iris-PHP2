<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */


/**
 * This helper will generate a 
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ */
class MagikLinx extends \Iris\views\helpers\_ViewHelper {

    protected $_modelClass = '\models\TLinks';

    public function help($name = \NULL, $internalFN = 'Internal', $labelFN = 'Label', $titleFN = 'Title', $URLFN = 'URL') {
        if (is_null($name)) {
            return $this;
        }
        $modelClass = $this->_modelClass;
        list($internal, $label, $title, $url) = $modelClass::GetMagikLink($name, $internalFN, $labelFN, $titleFN, $URLFN);
        $classInternal = $internal ? ' class="internal"' : '';
        if ($url == '') {
            $goodName = sprintf('<span class="magiklinx" title="%s">%s</span>', $title, $label);
        }
        else {
            $goodName = sprintf('<span class="magiklinx" title="%s"><a %s href="%s">%s</a></span>', $title, $classInternal, $url, $label);
        }
        return $goodName;
    }

    /**
     * Permits to change the default model class (normally \models\TLinks)
     * 
     * @param string $modelClass
     * @return \Iris\views\helpers\MagikLinx for fluent interface
     */
    public function setModelClass($modelClass) {
        $this->_modelClass = $modelClass;
        return $this;
    }

}
