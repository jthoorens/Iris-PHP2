<?php

namespace Iris\controllers\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Helper which creates a PopWindow function in header
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ */
class PopUp extends _ControllerHelper {

    public function help() {
        $script = <<<STOP
function PopUpWindow(url,name,details) {
window.open(url,name,details)
}
STOP;
        $this->_controller->callViewHelper('javascriptLoader','PopUpWindow',$script);
    }

}

