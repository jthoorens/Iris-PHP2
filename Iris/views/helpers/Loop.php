<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Loop offers a way to display the items of an array/object by means of a
 * partial.
 * 
 */
class Loop extends _ViewHelper {

    
    public function help($viewName, $dataSet) {
        $loop = new \Iris\MVC\Loop($viewName,$dataSet);
        return $loop->render();
    }

}

