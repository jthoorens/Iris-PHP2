<?php

namespace modules\syntax\controllers;

/**
 * Project : srv_www_IrisWB
 * Created for IRIS-PHP 1.0 RC2
 * Description of _syntax
 * 
 * @author 
 * @license 
 */
abstract class _syntax extends \modules\_application {

    
    /**
     * This method can contain module level
     * settings
     */
    protected final function _moduleInit() {
        // You should modify this demo layout
        $this->_setLayout('main');
        $this->__bodyColor = "#88F";
        // these parameters are only for demonstration purpose
        // and required by the default layout defined in _syntax
        $this->__buttons = 1 + 4;
        $this->__logoName = 'mainLogo';
    }

}
