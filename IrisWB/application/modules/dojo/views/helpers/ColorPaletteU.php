<?php

namespace application\modules\dojo\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * This is part of the WorkBench fragment
 *  
 * @copyright 2011-2018 Jacques THOORENS
 */

/*
 * This code is just an example of the use of a Dojo resource
 * Use of the resource to have a concrete modification in the page
 * 
 */

class ColorPaletteU extends \Iris\views\helpers\_ViewHelper {

    const MODE7_10 = 0;
    const MODE3_4 = 1;

    /**
     * 
     * @param string $objectID The object id to change the color of
     * @param int $mode The palette size
     * @return string
     */
    public function help($objectID, $mode = self::MODE7_10) {
        if (is_numeric($mode)) {
            $mode = $mode ? '3x4' : '7x10';
        }
        $id = \Dojo\Engine\Bubble::NewObjectName('CP');
        $bubble = \Dojo\Engine\Bubble::GetBubble($id);
        $bubble->addModule('dijit/ColorPalette','ColorPalette');
        $bubble->addModule('dojo/dom-style','style');
        $bubble->defFunction(<<<CODE
                
   var myPalette = new ColorPalette({
        palette: "$mode",
        onChange: function(val){style.set('$objectID','backgroundColor',val); }
    }, "$id");       
CODE
        );
        $code = "<span id=\"$id\">this will be replaced</span>";
        return $code;
    }

}
