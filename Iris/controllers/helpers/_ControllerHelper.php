<?php

namespace Iris\controllers\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * A controller helper is a sort of method usable by all controllers.
 * The main difference with the a proper method is that it has no access
 * to non public methods and variables.
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ * 
 */
abstract class _ControllerHelper extends \Iris\MVC\_Helper {//*/
    // By default all controller helper are singleton

    protected static $_Singleton = TRUE;

    protected static $_Type =  \Iris\Engine\Loader::CONTROLLER_HELPER;   
    
    protected static $_Message = "Error while executing controller helper %s: %s ";
    /**
     *
     * @var \Iris\MVC\_Controller 
     */
    protected $_controller;

    /**
     * A convenient way of calling a controller helper by its name
     * 
     * @param string $functionName the controller helper name (as a method name)
     * @param mixed[] $arguments the ar
     * @param \Iris\MVC\_Controller $controller : the calling controller 
     * @return type 
     */
//    public static function HelperCall($functionName, $arguments, $controller) {
//        //i_dnd('controller helper');
//        try {
//            $object = parent::GetObject($functionName, \Iris\Engine\Loader::CONTROLLER_HELPER);
//            $object->_controller = $controller;
//        }
//        catch (\Exception $ex) {
//            self::_DisplayException($functionName,$ex);
//        }
//        return call_user_func_array(array($object, 'help'), $arguments);
//    }

    /**
     * The controller helper can access to public methods of its calling controller
     * 
     * @param string $name
     * @param mixed[] $arguments
     * @return mixed 
     */
    public function __call($name, $arguments) {
        return call_user_func_array(array($this->_controller, $name), $arguments);
    }

    /**
     * The controller helper can get the value of a public variable of its calling controller
     * 
     * @param string $name
     * @return mixed
     * @todo test or delete
     */
    public function __get($name) {
        return $this->_controller->$name;
    }

    /**
     * The controller helper can modify the value of a public variable of its calling controller
     * 
     * @param string $name
     * @param mixed $value 
     * @todo test or delete
     */
    public function __set($name, $value) {
        $this->_controller->$name = $value;
    }

}
