<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Creates a "quoted" view , passes it an array of data and
 * "renders" it.
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ * 
 * 
 */
class Quote extends _ViewHelper {


    public function help($text, $data=NULL) {
        if(is_null($data)){
            $data = $this->_view;
        }
        $quoteView = new \Iris\MVC\Quote($text, $data);
        return $quoteView->render();
    }

}

