<?php

namespace Iris\MVC;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * An abstract helper is the superclass for controller helpers
 * and view helpers
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ * 
 */
abstract class _Helper { //implements \Iris\Translation\iTranslatable {

    use \Iris\Translation\tSystemTranslatable;

    /**
     *
     * @var AbstractActionHelper; 
     */
    protected static $_Instances = [];

    /**
     *
     * @var boolean
     */
    protected static $_Singleton = \FALSE;
    protected static $_Message = ""; //to be overwritten
    protected static $_Type = 0;

    /**
     * This abstract function is required, 
     * but its signature may vary from one implementation to another
     * so we cannot define it here
     */
    //public abstract function help();

    /**
     * Call to the help method of an helper (this method must be overriden
     * according to the different types of helpers)
     * 
     * @param string $functionName Class name of helper
     * @param mixed[] $arguments arguments for help method
     * @param mixed $caller the view/controller possibly containing the helper
     * @return mixed 
     */
    public static function HelperCall($functionName, $arguments = [], $caller = \NULL) {
        if (is_null($caller) and static::$_Type == \Iris\Engine\Loader::VIEW_HELPER) {
            $caller = new \Iris\MVC\View();
        }
        if (!is_array($arguments)) {
            $arguments = [$arguments];
        }
        try {
            $object = self::GetObject($functionName, static::$_Type);
            $object->_controller = $caller;
        }
        catch (\Exception $exception) {
            self::_DisplayException($functionName, $exception);
        }
        return call_user_func_array(array($object, 'help'), $arguments);
    }

//    public static function HelperCall($functionName, $arguments = [], $controller) {
//        try {
//            $object = parent::GetObject($functionName, \Iris\Engine\Loader::CONTROLLER_HELPER);
//            $object->_controller = $controller;
//        }
//        catch (\Exception $ex) {
//            $helperName = ucfirst($functionName);
//            $errorMessage = $ex->getMessage();
//            $file = $ex->getFile();
//            $line = $ex->getLine();
//            $errorMessage .= "<br/> in file <b>$file</b> line <b>$line</b>";
//            $exception = new \Iris\Exceptions\HelperException("Error while executing controller helper $helperName: $errorMessage ");
//            throw $exception;
//        }
//        return call_user_func_array(array($object, 'help'), $arguments);
//    }
//    public static function HelperCall($functionName, $arguments = [], $view = \NULL) {
//        try {
//            if (is_null($view)) {
//                $view = new \Iris\MVC\View();
//            }
//            if (!is_array($arguments)) {
//                $arguments = array($arguments);
//            }
//            $object = self::GetObject($functionName, \Iris\Engine\Loader::VIEW_HELPER);
//            $object->setView($view);
//        }
//        catch (\Exception $ex) {
//            $helperName = ucfirst($functionName);
//            $errorMessage = $ex->getMessage();
//            $file = $ex->getFile();
//            $line = $ex->getLine();
//            $errorMessage .= "<br/> in file <b>$file</b> line <b>$line</b>";
//            $exception = new \Iris\Exceptions\HelperException("Error while executing view helper $helperName: $errorMessage ");
//            throw $exception;
//        }
//        return call_user_func_array(array($object, 'help'), $arguments);
//    }

    /**
     * The protected construct may be completed in subclasses through
     * additions made in init(). In the helpers, the constructor is not
     * explicitely called in a new
     */
    protected function __construct() {
        $this->_init();
    }

    /**
     * Permits to modify the constructor behavior
     */
    protected function _init() {
        
    }

    /**
     *
     * @param string $functionName
     * @param string $helperType
     * @return \Iris\MVC\_Helper 
     */
    protected static function GetObject($functionName, $helperType) {
        //i_dnd("Name : ".$functionName);
        $segments = explode('_', $functionName);
        foreach ($segments as &$item) {
            $item = ucfirst($item);
        }
        $size = count($segments);
        // a function name 
        if ($size === 1) {
            $library = 'Iris';
            $className = $segments[0];
        }
        // a library + a function name 
        else if ($size === 2) {
            $library = $segments[0];
            $className = $segments[1];
        }
        // a complex library + a function name 
        else {
            $className = array_pop($segments);
            $library = implode("\\", $segments);
        }
        $loader = \Iris\Engine\Loader::GetInstance();
        if ($helperType === \Iris\Engine\Loader::VIEW_HELPER) {
            $completeClassName = "$library\\views\\helpers\\" . $className;
        }
        else {
            $completeClassName = "$library\\controllers\\helpers\\" . $className;
        }
        $foundClass = $loader->loadHelper($completeClassName, $helperType);
        if (class_exists("$foundClass", \FALSE)) {
            //print("{$foundClass} exists");
            $object = $foundClass::GetInstance();
            }
        else {
            i_dnd("{$foundClass} Does not exist");
            $object = $completeClassName::GetInstance();
            //die('LoadHelper');
        }
        return $object;
    }

    /**
     *
     * @return static 
     */
    public static function GetInstance() {
        // late binding class name
        $className = get_called_class();
        if (!static::$_Singleton) {
            // Pas singleton : nouvelle instance
            $instance = new $className();
        }
        else {
            // singleton : gets existing instance or creates one
            if (!isset(self::$_Instances[$className])) {
                self::$_Instances[$className] = new $className();
            }
            $instance = self::$_Instances[$className];
        }
        return $instance;
    }

    protected static function _DisplayException($functionName, $exception) {
        $helperName = ucfirst($functionName);
        $errorMessage = $exception->getMessage();
        $file = $exception->getFile();
        $line = $exception->getLine();
        $errorMessage .= "<br/> in file <b>$file</b> line <b>$line</b>";
        //$message = "Error while executing controller helper %s: %s ";
        $exception = new \Iris\Exceptions\HelperException(sprintf(static::$_Message, $helperName, $errorMessage));
        throw $exception;
    }

    protected static function _DisplayExceptionV($functionName, $exception) {
        i_d(get_called_class());
        $helperName = ucfirst($functionName);
        $errorMessage = $exception->getMessage();
        $file = $exception->getFile();
        $line = $exception->getLine();
        $errorMessage .= "<br/> in file <b>$file</b> line <b>$line</b>";
        $message = "Error while executing view helper %s : %s";
        $exception = new \Iris\Exceptions\HelperException(sprintf($message, $helperName, $errorMessage));
        throw $exception;
    }

}
