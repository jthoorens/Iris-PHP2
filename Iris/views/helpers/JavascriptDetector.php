<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */


/**
 * This helper is part of the javascript detection mechanism of IRIS-PHP.
 * It adds a noscript part in the head part of the view/layout file
 * while no 'iris_nojavascript' variable exist in $_SESSION.
 * If the client has no javascript, a refresh is done to jsTest.php
 * which will create the variable before returning to the site root.
 * 
 */
class JavascriptDetector extends _ViewHelper {

    protected static $_Singleton = TRUE;

    public function help($command='/!iris/reset/jsTest') {
        if (\Iris\Users\Session::JavascriptEnabled()) {
            echo <<<END
<noscript>
            <meta http-equiv="refresh" content="1; URL=$command"/>
        </noscript> 

END;
        }
    }
    

}

