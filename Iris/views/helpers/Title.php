<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Gives facilities to manage the &gt;title> of a page.
 * Subtitle can be managed more easily by subtitle() helper.
 *
 */
final class Title extends _ViewHelper {

    const BROL = "Brol";
    protected static $_Singleton = TRUE;

    /**
     * Permits to set the main title and the subtitle of the page
     * To set the subtitle only use the subtitle() helper instead.
     * 
     * @param string $mainTitle
     * @param string $subtitle
     */
    public function help($mainTitle = \NULL, $subtitle = \NULL) {
        $head = \Iris\Subhelpers\Head::GetInstance();
        if (!is_null($mainTitle)) {
            $head->setTitle($mainTitle);
        }
        if (!is_null($subtitle)) {
            $head->setSubtitle($subtitle);
        }
    }

}

