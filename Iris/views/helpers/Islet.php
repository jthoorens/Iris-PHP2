<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Provides an interface to fire an islet in a view/layout. Initializes <ul>
 * <li> the islet name
 * <li> the parameters (optional)
 * <li> the action name (def: index)
 * <li> the module name (def: same as that provided by the router)
 * </ul>
 * 
 */
class Islet extends _ViewHelper {

    public function help($isletName, $parameters = [], $actionName = 'index', $moduleName = \NULL) {
        // creates another response
        $response = \Iris\Engine\Response::GetOtherInstance($isletName, $actionName, $moduleName);
        // creates and uses the islets after verifying the parameters

        /* @var $islet \Iris\MVC\_Islet */
        $islet = $response->makeController();
        if (is_array($parameters)) {
            $islet->setParameters($parameters);
        }
        else {
            throw new \Iris\Exceptions\BadParameterException('The parameters transmited to an islet must be in an array');
        }
        // acts as a dispatcher
        $islet->preDispatch();
        $islet->excecuteAction();
        $output = $islet->dispatch(\FALSE);
        $islet->postDispatch();
        return $output;
    }

}
