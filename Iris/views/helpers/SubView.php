<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Place holder for the rend
 * 
 */
class SubView extends _ViewHelper {

    
    public function help($number) {
        if(! $this->_view instanceof \Iris\MVC\Layout){
           throw new \Iris\Exceptions\ViewException('Subviews appear only in a layout, not in other types of view.'); 
        }
        $subview = $this->_view->getSubView($number);
        if(is_null($subview)){
            throw new \Iris\Exceptions\ViewException("No subview with number $number found");
        }
        return $subview;
        
    }

}

