<?php

namespace application\modules\db\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2018 Jacques THOORENS
 */



/**
 * Displays the database state and warning messages
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ */
class DbWarning extends \Iris\views\helpers\_ViewHelper {

       
    protected $_singleton = TRUE;

    /**
     * Returns the status of the database
     * 
     * @return string
     */
    public function help($state = \NULL) {
        $currentState = \Iris\controllers\helpers\_ControllerHelper::HelperCall('dbState',[],$this->_view)->state($state);
        list($warningText, $warningClass) = $currentState;
        return <<< WARNING
<p class="dbWarning $warningClass">
    $warningText   
</p>
WARNING;
        }

    
}
