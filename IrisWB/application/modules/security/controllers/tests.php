<?php

namespace modules\security\controllers;

/**
 * Project : srv_www_IrisWB
 * Created for IRIS-PHP 1.0 RC2
 * Description of tests
 * 
 * @author 
 * @license 
 */
class tests extends _security {

    protected function _init() {
        $this->setViewScriptName('all');
    }

    public function debugAction() {
        $this->__filters = filter_list();
    }

    public function tocAction() {
// these parameters are only for demonstration purpose
        $this->__options = [
            ['$_COOKIE', "cookie", ""],
            ['$_ENV', "env", ""],
            ['$_FILE', "file", ""],
            ['$_GET', "get", ""],
            ['$_POST', "post", ""],
            ['$_REQUEST', "request", ""],
            ['$_SERVER', "server", ""],
            ['$_SESSION', "session", ""],
        ];
        $this->setViewScriptName('');
    }

    public function cookieAction($debug = \TRUE) {
        if (\modules\security\views\helpers\Display::TEST == 0) {
            die('OK');
        }
        if(Aspect::$_singleton){
            
        }
        if ($debug) {
            $this->__all = $_COOKIE;
            $this->__action = 'cookie';
            $this->setViewScriptName('all');
        }
        else {
            
        }
    }

    public function envAction($debug = \TRUE) {
        if ($debug) {
            $this->__action = 'env';
            $this->__all = $_ENV;
            $this->setViewScriptName('all');
        }
        else {
            
        }
    }

    public function fileAction($debug = \TRUE) {
        if ($debug) {
            $this->__action = 'file';
            $this->__all = $_FILES;
            $this->setViewScriptName('all');
        }
        else {
            
        }
    }

    public function getAction($debug = \TRUE) {
        if ($debug) {
            $this->__action = 'get';
            $this->__all = $_GET;
            $this->setViewScriptName('all');
        }
        else {
            
        }
    }

    public function postAction($debug = \TRUE) {
        if ($debug) {
            $this->__action = 'post';
            $this->__all = $_POST;
            $this->setViewScriptName('all');
        }
        else {
            
        }
    }

    /**
     * Request is not available because it is too ambiguous
     */
    public function requestAction() {
        $this->setViewScriptName('');
        $this->__all = [];
        $this->_setLayout('error');
        $this->__action = 'request';
//        \modules\security\views\helpers\Display::FunctionCall();
    }

    public function serverAction($debug = \TRUE) {
        if ($debug) {
            $this->__action = 'Server';
            $this->__all = $_SERVER;
            $this->setViewScriptName('all');
        }
        else {
            
        }
    }

    public function sessionAction($debug = \TRUE) {
        if ($debug) {
            $this->__action = 'session';
            $this->__all = $_SESSION;
            $this->setViewScriptName('all');
        }
        else {
            
        }
    }

}
