<?php

namespace application\modules\db\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2018 Jacques THOORENS
 */

/**
 * A localized formater for a date and time: eg. 5 mars - 13:15
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ */
class SimpleTimeDate extends \Iris\views\helpers\_ViewHelper {

    protected $_singleton = TRUE;

    /**
     * Returns a localized formated date: e.g. 5 janvier 2013
     * 
     * @param \Iris\Time\TimeDate/string $dateTime
     * @return string
     */
    public function help($dateTime) {
        if(is_string($dateTime)){
            $dateTime = new \Iris\Time\TimeDate($dateTime);
        }
        return $dateTime->toString('j F - G:i');
    }

}
