<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * A helper for image display
 *  
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ * 
 */
class Image extends _ViewHelper {

    protected $_folder = BLANKSTRING;
    protected $_defaultClass = '';
    


    /**
     * Creates an image tag with alt and title
     * 
     * @param string $source Image file name
     * @param string $alt Alt attribute (to display if image is missing)
     * @param string $tooltip Title attribute (tooltip)
     * @param string $folder image directory (by default images)
     * @param string $class class name for CSS
     * @param string $id optional id for the object
     * @return string 
     */
    public function help($source = BLANKSTRING, $alt = 'Image', $tooltip = BLANKSTRING, $folder = BLANKSTRING, $class = '', $id = BLANKSTRING) {
        /* @var $image \Iris\Subhelpers\ImageLink */
        $args = func_get_args();
        return new \Iris\Subhelpers\Image($args);
    }

    
}

