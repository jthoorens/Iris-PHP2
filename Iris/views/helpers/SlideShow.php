<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * This helpers realizes a slide show.
 *
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ * 
 */
class SlideShow extends _ViewHelper {

    /**
     * SlideShow is an singleton
     * @var boolean
     */
    protected static $_Singleton = TRUE;
    
   
    

    
    /**
     * The main help method only returns the unique instance of the 
     * default subhelper (Dojo implementation by default)
     * 
     * @return \Dojo\views\helpers\SlideShow
     */
    public function help($objectName = 'SlideShow1') {
        return \Iris\Subhelpers\_SlideShowManager::GetSlideShow($objectName);
    }

    
}

