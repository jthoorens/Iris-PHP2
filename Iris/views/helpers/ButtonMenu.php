<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Helper for rendering menus as a html <ul><li> recursive list
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ * @todo definir un subhelper
 */
class ButtonMenu extends Menu {

    protected $_activeClass;
    
    protected $_mainTag;
    
    /**
     * Inits the active class and main tag names from settings
     */
    protected function _init() {
        $this->_activeClass = \Iris\SysConfig\Settings::$MenuActiveClass;
        $this->_mainTag = \Iris\SysConfig\Settings::$ButtonMenuMainTag;
    }

    protected function _renderItem($item,$dummy) {
        $uri = $this->_simplifyUri($item['uri']);
        $label = $this->_($item['label']);
        $title = $this->_($item['title']);
        return $this->callViewHelper('button',$label,$uri,$title);
    }

}

