<?php
namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * This helper returns "mailto" link in which all characters are
 * replaced by random representation : letters, decimal and
 * hexadecimal numbers. The description is in clear unless it
 * is the same as the email address. The intention is prevent
 * robots from using email address to send spam.
 * 
 * CAUTION : this routine is not utf8-safe.
 * @todo: rewrite an UTF8 version of this helper to treat new non ascii domains.
 * 
 */
class SecureMail extends _ViewHelper {

    
    public function helper($addresse,$description=\NULL){
        return 'Email';
        if(is_null($description)){
            $description=$this->obfuscate($addresse);
        }
        return '<a href="'.$this->obfuscate('mailto:').$this->obfuscate($addresse).'">'.$description.'</a>';
    }


    /**
     * Generates an obfuscated version of an email address.
     *
     * @author     Kohana Team
     * @copyright  (c) 2007-2008 Kohana Team
     * @license    http://kohanaphp.com/license.html
     * @filesource Kohana 2.2 : /system/helpers/html
     *
     * @param   string  email address
     * @return  string
     */
    public function obfuscate($email)
    {
        $safe = '';
        foreach (str_split($email) as $letter)
        {
            switch (($letter === '@') ? rand(1, 2) : rand(1, 3))
            {
                // HTML entity code
                case 1:
                    $safe .= '&#'.ord($letter).';';
                    break;
                // Hex character code
                case 2:
                    $safe .= '&#x'.dechex(ord($letter)).';';
                    break;
                // Raw (no) encoding
                case 3:
                    $safe .= $letter;
                    break;
            }
        }

        return $safe;
    }

}

