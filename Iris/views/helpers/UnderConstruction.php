<?php


namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Returns an html text consisting of a localized title "Page under construction"
 * and a logo representing works in progres
 *
 */ 
class UnderConstruction extends \Iris\views\helpers\_ViewHelper{

    
    public function help(){
        $html = "<h3>".$this->_('Page under construction',TRUE)."</h3>\n";
        $html .= $this->callViewHelper('image','construction.png',
                $this->_('Page under construction',TRUE),NULL,'/!documents/file/images');
        return $html;
    }
    

}


