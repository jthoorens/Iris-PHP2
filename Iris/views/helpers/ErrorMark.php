<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * An example of view helper (for Iris WB)
 * 
 */
class ErrorMark extends \Iris\views\helpers\_ViewHelper {

    static $_singleton = FALSE;

    public function help($text) {
        if (\Iris\Errors\Handler::IsProduction())
            return '';
        else
            return <<< HTML
        <div style="color:grey;text-align:center;font-size:0.8em">$text</div>
HTML;
    }

}
