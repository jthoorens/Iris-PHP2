<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * This helper displays all debugging messages that have been
 * recorded during page generation. It has no effect in 
 * production environement. It should be placed  at the end
 * of the layout (it uses the CSS 'log' clas predefined in ILO
 * collection).
 * 
 */
class LogDisplay extends _ViewHelper {

    
    public function help() {
        if (\Iris\Engine\Mode::IsDevelopment()) {
            $logMessages = \Iris\Engine\Log::GetInstance()->render();
            if ($logMessages != '') {
                return "<div class=\"log\">\n$logMessages\n</div>";
            }
        }
    }

}


