<?php

namespace Iris\views\helpers;

use Iris\Subhelpers\Link as SubLink;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 *
 *
 * Creates a button which links to a page or site
 *
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ */
class SecureLink extends \Iris\views\helpers\_ViewHelper {

    /**
     * @param string $label link label
     * @param string $url target URL
     * @param string $tooltip tooltip when mousevoer
     * @param string $class class of the link
     * @param string $id id of the linklabel
     * @return string
     */
    public function help($tag = \NULL, $label = \NULL, $url = \NULL, $tooltip = \NULL, $class = \NULL, $id = \NULL) {
        $acl = \Iris\Users\Acl::GetInstance();
        $controller = dirname($url);
        $action = basename($url);
        if ($acl->hasPrivilege($controller, $action)) {
            $subhelper = SubLink::GetInstance();
            $args = func_get_args();
            array_shift($args);
            $link = $subhelper->autoRender($args);
            return "<$tag>".$link."</$tag>";
        }
        else {
            return '';
        }
    }

}
