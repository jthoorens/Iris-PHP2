<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * A project of link to FaceBook
 * 
 */
class Facebook extends _ViewHelper {

    public function help($command=NULL) {
        if ($command == NULL) {
            return $this->_render();
        }
        else{
            return $this->_js();
        }
    }

    private function _js() {
        $text = <<< JS
        (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
JS;
        $this->callViewHelper('javascriptLoader','Facebook',$text);
    }

    private function _render() {
        return <<< RENDER
   <div class="fb-like" data-href="http://www.facebook.com/pages/IrisPHP/373632602651651" 
       data-send="true" data-width="450" data-show-faces="true"></div>
RENDER;
        
    }
}

    