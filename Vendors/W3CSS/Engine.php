<?php

namespace Vendors\W3CSS;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * The engine for the W3CSS library
 *
 * Project IRIS-PHP
 *
 * @author Jacques THOORENS (irisphp@thoorens.net)
 */
trait Engine {

    public function _init(){
        Settings::$DownloadMode = Settings::PUBLICDIR;
        $this->libraryLoader();
    }
    
    public function libraryLoader() {
        switch(Settings::$DownloadMode){
            case Settings::DISTANT:
                $url = Settings::$W3CURL;
                break;
            case Settings::LOCAL:
                $url = Settings::$LocalURL;
                break;
            case Settings::PUBLICDIR:
                $url = Settings::$PublicURL;
                break;
        }
        \Iris\views\helpers\_ViewHelper::HelperCall("styleLoader", $url);
        //\Iris\views\helpers\StyleLoader::FunctionCall($url);
    }

}
