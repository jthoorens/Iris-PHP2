<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * This helper displays anything passed as parameteter as ascii text, 
 * ignoring html tag.
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ * @todo : verify the utility of this and suppress it in all layouts
 */
class ShowHtml extends _ViewHelper {

    /**
     * 
     * @param string $data The text to display
     * @param boolean $cut If true, each space is replaced by CRLF
     * @param text $class a class name to qualify the embedded &lt;pre>
     * @return string
     */
    public function help($data, $cut = \FALSE, $class = '') {
        $classAttribute = $class == '' ? '' : " class=\"$class\""; 
        $html = "<pre$classAttribute>\n";
        $formattedData = str_replace('<', '&lt', $data);
        if($cut){
            $formattedData =str_replace(' ', "\n", $formattedData);
        }
        $html .= $formattedData;
        $html .= "</pre>";
        return $html;
    }

}

