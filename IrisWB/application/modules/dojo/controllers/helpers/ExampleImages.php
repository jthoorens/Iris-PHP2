<?php

namespace application\modules\dojo\controllers\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2018 Jacques THOORENS
 */

/**
 * 
 */
class ExampleImages extends \Iris\controllers\helpers\_ControllerHelper {

    /**
     * Creates an array containing data for getting 5 images
     * 
     * @param boolean $jsonFormat If true, converts the array to JSON
     * @return string/array a JSON string or an array
     */
    public function help($jsonFormat = \TRUE) {
        $titles = ['Etretat (France)', 'Pastry (Alsace France)', 'Lama', 'Tramway (Lisboa)', 'Cabourg (France)'];
        for ($i = 1; $i < 6; $i++) {
            $images[] = [
                "large" => sprintf("/images/slideshow/image%02d.jpg", $i),
                "title" => $titles[$i - 1],
            ];
        }
        return $images;
    }

}