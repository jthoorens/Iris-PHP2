<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * A helper for sound in html5
 *  
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ * 
 */
class Audio5 extends _ViewHelper {

    const ALL = -1;
    const MP4 = 1;
    const MP3 = 2;
    const OGG = 4;
    const WAV = 8;
    
    public function help($fileName = NULL, $id='', $mode = self::ALL, $autoplay = \TRUE, $controls = \TRUE) {
        if(is_null($fileName)){
            return $this;
        }
        else{
            return $this->render($fileName, $id, $mode, $autoplay, $controls);
        }
    }
    
    public function render($fileName, $id, $mode = self::ALL, $autoplay = \TRUE, $controls = \TRUE) {
        $controlsAttribute = $controls ? ' controls ' : '';
        $autoplayAttribute = $autoplay ? ' autoplay ' : '';
        $html = "<audio id=\"$id\" $controlsAttribute $autoplayAttribute >\n";
        if($mode & self::MP4){
            $html .= "\t <source src=\"$fileName.aac\" type=\"audio/mp4\">\n";
        }
        if($mode & self::MP3){
            $html .= "\t <source src=\"$fileName.mp3\" type=\"audio/mp3\">\n";
        }
        if($mode & self::OGG){
            $html .= "\t <source src=\"$fileName.ogg\" type=\"audio/ogg\">\n";
        }
        if($mode & self::WAV){
            $html .= "\t <source src=\"$fileName.wav\" type=\"audio/wav\">\n";
        }
        $html .= "</audio>\n";
        return $html;
    }

}

