<?php

namespace application\modules\security\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2018 Jacques THOORENS
 */

/**
 * Presenting an invoice
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * 
 */
class Display extends \Iris\views\helpers\_ViewHelper {

    const TEST = 0;
    /**
     * Returns a description of an invoice
     * 
     * @param \Iris\DB\Object $invoice
     * @param mixed $id
     * @return string
     */
    public function help($value) {
        $type = gettype($value);
        switch ($type) {
            case 'string':
            case 'integer':
            case 'double':
                $returnValue = "($type) ".$value;
                break;
            default:
                $returnValue = 'Not printable '.$type;
                break;
        }
        return $returnValue;
    }

}
