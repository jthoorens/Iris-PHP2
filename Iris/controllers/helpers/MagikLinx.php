<?php

namespace Iris\controllers\helpers;
/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * replaces all &lt;* tags *> by {magiklinx(....)}
 * to produce special links and tooltip
 *
 * More explanation in \Iris\views\helpers\MagikLinx
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 */
class MagikLinx extends \Iris\controllers\helpers\_ControllerHelper{
    
    public function help($text){
        return preg_replace('/(<\*)(.*?)(\*>)/', '{magikLinx(\'$2\')}', $text);
    }
}

?>
