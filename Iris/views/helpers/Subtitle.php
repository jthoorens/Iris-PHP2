<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Gives facilities to manage the <title> of a page.
 *
 */
final class Subtitle extends _ViewHelper {

    protected static $_Singleton = TRUE;

    public function help($text) {
        $head = \Iris\Subhelpers\Head::GetInstance();
        $head->setSubtitle($text);
    }


}


