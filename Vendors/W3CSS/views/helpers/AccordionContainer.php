<?php

namespace Vendors\W3CSS\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * This helper permits to use Accordion containers of Dojo. If javascript
 * is not available on the client, it simulate the tab with buttons and interaction
 * with the server. Another option is to display all the items, with <h5> title in front
 * of them.
 *

 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ * 
 */
class AccordionContainer extends \Iris\views\helpers\_ViewHelper{

    use \Vendors\W3CSS\Engine;

    protected $_closeColor = 'w3-black';
    protected $_openColor = 'w3-red';
    protected $_align = 'w3-left-align';
    protected $_functionName = 'myFunction';

    public function help($text = \NULL) {
        i_d($text);
        if ($text === \NULL) {
            $returnValue = $this;
        }
        else {
            $returnValue = '<div>My accordeon</div>';
        }
        return $returnValue;
    }

    public function setCloseColor($closeColor) {
        $this->_closeColor = $closeColor;
        return $this;
    }

    public function setOpenColor($openColor) {
        $this->_openColor = $openColor;
        return $this;
    }

    public function setAlign($align) {
        $this->_align = $align;
        return $this;
    }

    public function setFunctionName($functionName) {
        $this->_functionName = $functionName;
        return $this;
    }

    public function item($name, $title, $content) {
        $returnValue = <<< MYITEM
<button onclick="$this->_functionName('$name')" class="w3-button w3-block $this->_closeColork $this->_align">$title</button>
<div id="$name" class="w3-hide w3-container">
    $content
</div>
MYITEM
        ;
        return $returnValue;
    }

}
