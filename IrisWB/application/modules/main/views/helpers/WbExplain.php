<?php

namespace application\modules\main\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */
//namespace Iris\views\helpers;


/**
 * Explanation text in Workbench
 * 
 */
class WbExplain extends \Iris\views\helpers\_ViewHelper{
    
    public function help($view){
        $sequence = \wbClasses\DBSequence::GetInstance();
        return $sequence->getContext($view, 'workbench\\Context')."\n<p />\n";
    }
    
    
    
    
}


