<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * This helper must be employes once in every layout to indicate
 * the insertion place of the action display in the page.
 * 
 */
class MainView extends \Iris\views\helpers\_ViewHelper {

    

    public function help() {
        $mainView = $this->getView()->getMainview();
        return $mainView->render();
    }

}

