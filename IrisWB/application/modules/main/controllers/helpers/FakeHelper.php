<?php

namespace application\modules\main\controllers\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2018 Jacques THOORENS
 */

/**
 * A helper which creates view variables just for fun
 * 
 */
class FakeHelper extends \Iris\controllers\helpers\_ControllerHelper{
    
    public function help(){
        $this->toView('var11','value 11');
        $this->__('var12','value 12');
        $this->__(\NULL, [
           'var13' => 'value 13',
            'var14' => 'value 14'
        ]);
        // View is protected : ILLEGAL
        //$this->_controller->_view->var15 = 'value 15';
        // Legal, but creates a variable in controller not in view
        $this->__view16 = 'value 16';
        
        // A complex init
        $controller = \Iris\Engine\Response::GetDefaultInstance()->makedController;
        $controller->__var17 = 'value 17';
        
    }
}


