<?php

namespace application\modules\main\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Information about script names in workbench
 * 
 */
class WbScriptName extends \Iris\views\helpers\_ViewHelper {


    public function help($requiredScript,$objectType = "Script"){
        $usedScript = str_replace(IRIS_ROOT_PATH, '', $this->_view->GetLastScriptName());
        return <<< END
    <table border=2>
        <tr><th>$objectType required</th><td>$requiredScript</td></tr>
        <tr><th>$objectType used</th><td>$usedScript</td></tr>
    </table>
END;
        
        
    }

}

