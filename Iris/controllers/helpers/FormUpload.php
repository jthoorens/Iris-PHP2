<?php

namespace Iris\controllers\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * A helper that creates an upload form
 * 
 * @author Jacques THOORENS (irisphp@thoorens.net)
 * @see http://irisphp.org
 * @license GPL version 3.0 (http://www.gnu.org/licenses/gpl.html)
 * @version $Id: $ */
class FormUpload extends _ControllerHelper{

    
    /**
     *
     * @return \Iris\Form\Form 
     */
    public function help() {
        $ff = \Iris\Forms\_FormFactory::GetFormFactory();
        $form = $ff->createForm('Loader');
        // NO UPLOAD WITHOUT THIS ATTRIBUTE !!
        $form->addAttribute('enctype',"multipart/form-data");

        new \Iris\Forms\PlaceHolder('_before_', $form);

        $ff->createFile('File',array('maxfilesize'=>'900000'))
                ->setLabel($this->_('File to upload:',TRUE))
                ->addTo($form)
                ->addValidator('Required');

        new \Iris\Forms\PlaceHolder('_after_', $form);

        
        $ff->createSubmit('Submite')
                ->setValue($this->_('Upload',TRUE))
                ->addTo($form);
        return $form;
    }

    

}

