<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * The error toolbar offers a series of buttons to 
 * manage the error display
 */
class ErrorToolbar extends \Iris\views\helpers\_ViewHelper {

    static $_singleton = FALSE;

    public function help() {
        $html = '';
        if (!\Iris\Errors\Handler::IsProduction()) {
            /* @var $settings \Iris\Errors\Settings */
            $html = sprintf("Flags : %b &diams; ", \Iris\Errors\Settings::GetErrorflags());
            /* @var $exception \Iris\Exceptions\_Exception */
            $exception = \Iris\Engine\Memory::Get('untreatedException');
            $router = \Iris\Engine\Router::GetInstance();
            $previous = $router->getPrevious();
            // in case of spontaneous error (e.g. privilege)
            if (is_null($previous)) {
                $oldURL = $router->getAnalyzedURI(\TRUE);
                $trace = \FALSE;
            }
            else {
                $oldURL = $previous->getAnalyzedURI(\TRUE);
                $trace = $exception->getTrace();
                $html .= $this->callViewHelper('button', 'REDO', "/$oldURL", "Repeat error in normal mode");
                $html .= $this->callViewHelper('button', 'HANG', "/$oldURL?ERROR=HANG", "Program will stop at first error.");
                $html .= $this->callViewHelper('button', 'NO WIPE', "/$oldURL?ERROR=KEEP", "All echoed text will be kept.");
                $html .= $this->callViewHelper('button', 'PRODSIM', "/$oldURL?ERROR=PRODSIM", "Simulate error in production mode.");
                $html .= ' &diams; Stack level:';
                for ($level = 0; $level < count($trace); $level++) {
                    $html .= $this->callViewHelper('button', "$level", "/$oldURL?ERRORSTACK=$level", "Display stack information from level " . $level);
                }
                $html .= $this->callViewHelper('button', "ALL", "/$oldURL?ERRORSTACK=-1", "Display stack information in tabs (depends on Dojo)");
                $html .= " &diams; ";
            }
        }
        return $html;
    }

}

