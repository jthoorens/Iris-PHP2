<?php

namespace application\modules\dojo\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * This is part of the WorkBench fragment
 *  
 * @copyright 2011-2018 Jacques THOORENS
 */

/*
 * This code is just an example of the use of a Dojo resource
 * Declaration version
 */

class ColorPaletteD extends \Iris\views\helpers\_ViewHelper {

    const MODE7_10 = 0;
    const MODE3_4 = 1;

    public function help($mode = self::MODE7_10) {
        if (is_numeric($mode)) {
            $mode = $mode ? '3x4' : '7x10';
        }
        $id = \Dojo\Engine\Bubble::NewObjectName('CP');
        $bubble = \Dojo\Engine\Bubble::GetBubble($id);
        $bubble->addModule('dijit/ColorPalette', 'ColorPalette');
        $code = <<<CODE
<div data-dojo-type="dijit/ColorPalette" 
    data-dojo-props="onChange:function(){alert(this.value);}, palette:'$mode'">
</div>
CODE;
        return $code;
    }

}
