<?php
namespace application\modules\main\views\helpers;

defined('CRLF') or define('CRLF', "\n");

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * Management of colors in workbench
 * 
 */
class WbHeader extends \Iris\views\helpers\_ViewHelper {

    static $_singleton = FALSE;

    public function help() {
        $html = '<div id="wbexplain">' . CRLF;
        $html .= $this->callViewHelper('wbExplain',$this);
        $html .= '</div>' . CRLF;
        return $html;
    }

}

