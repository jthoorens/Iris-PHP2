<?php

namespace Iris\views\helpers;

/*
 * This file is part of IRIS-PHP, distributed under the General Public License version 3.
 * A copy of the GNU General Public Version 3 is readable in /library/gpl-3.0.txt.
 * More details about the copyright may be found at
 * <http://irisphp.org/copyright> or <http://www.gnu.org/licenses/>
 *  
 * @copyright 2011-2017 Jacques THOORENS
 */

/**
 * FLoop offers a way to display the items of an array by means of a
 * function.
 * 
 */
class Floop extends _ViewHelper {

    private $_functions = [];
    protected static $_Singleton = \TRUE;

    public function help($viewName = \NULL, $dataSet = \NULL) {
        if (is_null($viewName)) {
            return $this;
        }
        else {
            if (isset($this->_functions[$viewName])) {
                $function = $this->_functions[$viewName];
            }
            else {
                $function = function($item, $key = \NULL) {
                    $context = " 
                    <div class=\"oldscreen old1\">
                        $key - $item
                </div>";
                    return $context;
                };
            };
        }
        $floop = new \Iris\MVC\FunctionLoop($function, $dataSet);
        return $floop->render();
    }

    /**
     * 
     * @param string $index
     * @param \Iris\views\helpers\callable $function 
     * @return string
     */
    /**
     * 
     * @param string $index
     * @param \callable $function
     * @return string
     */
    public function setFunction($index, $function) {
        $this->_functions[$index] = $function;
        return '';
    }

    

}
